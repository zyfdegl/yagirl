FROM golang:1.17 AS builder
WORKDIR /root/
ADD . ./
RUN make build

# stage 2

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /root/yagirl ./
CMD ["./yagirl"]