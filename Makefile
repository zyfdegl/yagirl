all: vend test cover

vend:
	go mod tidy && go mod vendor

test:
	go test -timeout=300s -cover -race -count=1 ./... -coverprofile cover.out

build:
	go build -o yagirl

cover:
	go tool cover -func cover.out

image:
	docker build -t zyfdedh/yagirl .