[![pipeline status](https://gitlab.com/zyfdegl/yagirl/badges/main/pipeline.svg)](https://gitlab.com/zyfdegl/yagirl/-/pipelines)
[![coverage report](https://gitlab.com/zyfdegl/yagirl/badges/main/coverage.svg)](https://gitlab.com/zyfdegl/yagirl/-/jobs)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/zyfdegl/yagirl)](https://goreportcard.com/report/gitlab.com/zyfdegl/yagirl)

# yagirl
Yet Another Go Implementation of RangeList.

RangeList is an aggregate of ranges, a pair of integers define a range.

e.g. [1, 5), [10, 11), [100, 201)

Add and remove operations can be done on a RangeList.

# Prerequisites
* go 1.12+
* make

# Have a try with Docker
```sh
docker run zyfdedh/yagirl
```

# Build in local
```sh
git clone https://gitlab.com/zyfdegl/yagirl.git

cd yagirl && make

make build
```

# Run
```sh
./yagirl
[1, 5)
[1, 5) [10, 20)
[1, 5) [10, 20)
[1, 5) [10, 21)
[1, 5) [10, 21)
[1, 8) [10, 21)
[1, 8) [10, 21)
[1, 8) [11, 21)
[1, 8) [11, 15) [17, 21)
[1, 3) [19, 21)
```