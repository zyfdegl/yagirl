package main

func main() {
	var rl RangeList = &RangeListMap{}

	rl.Add(NewRange(1, 5))
	rl.Print()

	rl.Add(NewRange(10, 20))
	rl.Print()

	rl.Add(NewRange(20, 20))
	rl.Print()

	rl.Add(NewRange(20, 21))
	rl.Print()

	rl.Add(NewRange(2, 4))
	rl.Print()

	rl.Add(NewRange(3, 8))
	rl.Print()

	rl.Remove(NewRange(10, 10))
	rl.Print()

	rl.Remove(NewRange(10, 11))
	rl.Print()

	rl.Remove(NewRange(15, 17))
	rl.Print()

	rl.Remove(NewRange(3, 19))
	rl.Print()
}
