package main

import "fmt"

// Range is a pair of integers representing one or more integers between index [0] and [1],
// to be clarify, each integer >= [0] && < [1].
//
// e.g. Range{1, 5} includes integers: 1, 2, 3, and 4.
//
// A valid range means L should less or equal than R.
type Range [2]int

// NewRange creates a Range with l, r
//
// Reversed inputs are allowed, in this case l, r is swapped.
func NewRange(l, r int) Range {
	if l > r {
		return [2]int{r, l}
	}
	return [2]int{l, r}
}

// L returns left bound of range r.
func (r Range) L() int {
	return r[0]
}

// R returns right bound of range r.
func (r Range) R() int {
	return r[1]
}

// Contains checks if r2 is a subset of r.
func (r Range) Contains(r2 Range) bool {
	if !r2.Valid() {
		return false
	}
	if r.L() <= r2.L() && r2.R() <= r.R() {
		return true
	}
	return false
}

// Empty checks if r has any valid ints, L==R is considered as empty
// because there's no ints in it.
func (r Range) Empty() bool {
	return r.L() >= r.R()
}

// Valid checks if L less or equal than R.
func (r Range) Valid() bool {
	return r.L() <= r.R()
}

// Equals checks if r and r2 have the same L and R bounds.
func (r Range) Equals(r2 Range) bool {
	return r.L() == r2.L() && r.R() == r2.R()
}

func (r Range) Sprint() string {
	// XXX optimize: use bufio
	return fmt.Sprintf("[%d, %d)", r.L(), r.R())
}

// Overlaps checks if r and r2 has any intersection,
// an empty range has no intersection with anyother range.
func (r Range) Overlaps(r2 Range) bool {
	if r.Empty() || r2.Empty() {
		return false
	}
	// same as !(non-overlapping)
	//
	// non-overlapping case 1
	//
	//      [L......R)
	//                   [L2.......R2)
	// ----------------------------------->
	// non-overlapping case 2
	//
	//      [L2......R2)
	//                    [L.......R)
	// ----------------------------------->
	//
	// return !(r.R() < r2.L() || r.L() > r2.R())

	// XXX strange here
	return r.R() >= r2.L() && r2.R() >= r.L()
}

// Merge merges r2 into r and then returns a new range, non-overlapping ranges cannot merge.
func (r Range) Merge(r2 Range) Range {
	if r2.Empty() {
		return r
	}
	if r.Empty() {
		return r2
	}
	// cannot merge non-overlapping ranges
	if !r.Overlaps(r2) {
		return Range{}
	}
	return Range{min(r.L(), r2.L()), max(r.R(), r2.R())}
}

// Cut removes the intersection of r and r2 from r, if r contains r2 then r can
// be cut into two.
func (r Range) Cut(r2 Range) []Range {
	if !r.Overlaps(r2) {
		return []Range{r}
	}
	// XXX simplify logic
	var pieces []Range
	if r.Contains(r2) {
		lcut := Range{r.L(), r2.L()}
		if !lcut.Empty() {
			pieces = append(pieces, lcut)
		}
		rcut := Range{r2.R(), r.R()}
		if !rcut.Empty() {
			pieces = append(pieces, rcut)
		}
		return pieces
	}
	//      [L........R)
	//           [L2.........R2)
	// ----------------------------------->
	if r.L() < r2.L() {
		return []Range{{r.L(), r2.L()}}
	}
	//           [L.........R)
	//      [L2........R2)
	// ----------------------------------->
	return []Range{{r2.R(), r.R()}}
}
