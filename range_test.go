package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRange(t *testing.T) {
	r := NewRange(1, 5)
	assert.Len(t, r, 2)
	assert.Equal(t, 1, r[0])
	assert.Equal(t, 5, r[1])

	// reversed input
	r = NewRange(5, 1)
	assert.Len(t, r, 2)
	assert.Equal(t, 1, r[0])
	assert.Equal(t, 5, r[1])
}

func TestRange_LR(t *testing.T) {
	var cases = []struct {
		input            Range
		expectL, expectR int
	}{
		{Range{}, 0, 0},
		{Range{1, 5}, 1, 5},
	}
	for i, v := range cases {
		assert.Equalf(t, v.expectL, v.input.L(), "failed on case %d", i)
		assert.Equalf(t, v.expectR, v.input.R(), "failed on case %d", i)
	}
}

func TestRange_Empty(t *testing.T) {
	var cases = []struct {
		input  Range
		expect bool
	}{
		{Range{}, true},
		{Range{0, 0}, true},
		{Range{1, 1}, true},
		{Range{1, 5}, false},
		{Range{5, 1}, true},
		{Range{-5, -1}, false},
	}
	for i, v := range cases {
		assert.Equalf(t, v.expect, v.input.Empty(), "failed on case %d", i)
	}
}

func TestRange_Contains(t *testing.T) {
	var cases = []struct {
		input1, input2 Range
		expect         bool
	}{
		{Range{}, Range{}, true},

		{Range{10, 20}, Range{1, 9}, false},
		{Range{10, 20}, Range{1, 10}, false},
		{Range{10, 20}, Range{1, 15}, false},
		{Range{10, 20}, Range{1, 20}, false},
		{Range{10, 20}, Range{1, 25}, false},

		{Range{10, 20}, Range{10, 10}, true},
		{Range{10, 20}, Range{10, 15}, true},
		{Range{10, 20}, Range{10, 20}, true},
		{Range{10, 20}, Range{10, 25}, false},

		{Range{10, 20}, Range{15, 15}, true},
		{Range{10, 20}, Range{15, 16}, true},
		{Range{10, 20}, Range{15, 20}, true},
		{Range{10, 20}, Range{15, 25}, false},

		{Range{10, 20}, Range{20, 20}, true},
		{Range{10, 20}, Range{20, 25}, false},

		{Range{10, 20}, Range{25, 25}, false},
		{Range{10, 20}, Range{25, 30}, false},

		{Range{-30, -20}, Range{-20, -10}, false},

		{Range{10, 20}, Range{25, 15}, false}, // invalid

	}
	for i, v := range cases {
		got := v.input1.Contains(v.input2)
		assert.Equalf(t, v.expect, got, "failed on case %d", i)
	}
}

func TestRange_Overlaps(t *testing.T) {
	var cases = []struct {
		input1, input2 Range
		expect         bool
	}{
		{Range{}, Range{}, false},
		{Range{5, 1}, Range{2, 3}, false}, // invalid

		{Range{10, 20}, Range{30, 40}, false},
		{Range{10, 20}, Range{20, 30}, true}, // ?
		{Range{10, 20}, Range{19, 30}, true},

		{Range{-30, -19}, Range{-20, -10}, true},
	}
	for i, v := range cases {
		got := v.input1.Overlaps(v.input2)
		assert.Equalf(t, v.expect, got, "failed on case %d", i)
		// vice versa
		got = v.input2.Overlaps(v.input1)
		assert.Equalf(t, v.expect, got, "failed on reversed case %d", i)
	}
}

func TestRange_Merge(t *testing.T) {
	var cases = []struct {
		input1, input2 Range
		expect         Range
	}{
		{Range{}, Range{}, Range{}},

		{Range{10, 20}, Range{20, 30}, Range{10, 30}}, // ?
		{Range{10, 20}, Range{10, 20}, Range{10, 20}},
		{Range{10, 20}, Range{15, 25}, Range{10, 25}},
		{Range{10, 20}, Range{10, 25}, Range{10, 25}},
		{Range{10, 20}, Range{9, 25}, Range{9, 25}},

		{Range{-20, -10}, Range{-15, 5}, Range{-20, 5}},
	}
	for i, v := range cases {
		got := v.input1.Merge(v.input2)
		assert.Equalf(t, v.expect, got, "failed on case %d", i)
		// vice versa
		got = v.input2.Merge(v.input1)
		assert.Equalf(t, v.expect, got, "failed on reversed case %d", i)
	}
}

func TestRange_Cut(t *testing.T) {
	var cases = []struct {
		input1, input2 Range
		expect         []Range
	}{
		{Range{}, Range{}, []Range{{0, 0}}},
		{Range{10, 21}, Range{10, 11}, []Range{{11, 21}}},
		{Range{10, 21}, Range{10, 21}, nil},
		{Range{17, 21}, Range{3, 19}, []Range{{19, 21}}},
	}
	for i, v := range cases {
		got := v.input1.Cut(v.input2)
		assert.Equalf(t, v.expect, got, "failed on case %d", i)
	}
}
