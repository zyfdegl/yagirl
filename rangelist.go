package main

type RangeList interface {
	Add(r Range) error
	Remove(r Range) error
	Print() error
}
