package main

import (
	"errors"
	"fmt"
	"math"
	"sort"
	"strings"
	"sync"
)

var (
	// assert implementation
	_ RangeList = &RangeListMap{}

	ErrLeftRangeNotFound = errors.New("left range not found")
)

// RangeListMap is an implement of RangeList, it uses a integer-keyed map of ranges
// to perform adding and removing operations.
type RangeListMap struct {
	// key: left bound of the range
	// value: the range
	mapping map[int]Range

	mu sync.Mutex
}

// Add adds range r to rl if no overlapping happens,
// if r overlaps with any range (multiple ranges) in rl, it will merge with it (them).
func (rl *RangeListMap) Add(r Range) error {
	rl.mu.Lock()
	defer rl.mu.Unlock()

	if r.Empty() {
		return nil
	}
	if len(rl.mapping) == 0 {
		rl.mapping = map[int]Range{
			r.L(): r,
		}
		return nil
	}
	// Considering there's only one range in rl, let's say, r0,
	// below in the coordinate it's [L0, R0), the adding operation can be done like
	//   1. check if there's any overlap between r and r0,
	//   2. if there's, then merge two,
	//   3. otherwise, add r to the map,
	//
	//          [L(r), R(r))
	//  -inf         [L0,     R0)         +inf
	// --------------------------------------->
	if len(rl.mapping) == 1 {
		var r0 Range
		for _, v := range rl.mapping {
			r0 = v
			break
		}
		if r0.Overlaps(r) {
			merged := r0.Merge(r)
			newKey := min(r0.L(), r.L())
			rl.mapping[newKey] = merged
			if newKey != r0.L() {
				delete(rl.mapping, r0.L())
			}
		} else {
			rl.mapping[r.L()] = r
		}
		return nil
	}

	// When it comes to multiple ranges in rl, we need to find the 'min range' and
	// 'max range' in rl, then do some merges and deletions.
	//
	// 'min range': the one with minimum L bound in all overlapping range(s) with r.
	// 'max range': the one with maximum L bound in all overlapping range(s) with r.
	//
	//   1. find the min range in rl overlaps with r, named R[m],
	//   2. find the max range in rl overlaps with r, named R[n],
	//   3. merge r with R[m]
	//   4. merge r with R[n]
	//     (if m==n, merge only once)
	//   5. delete elements between >m && <n
	//
	//               [L(r).........................R(r))
	//  -inf     [L(m), R(m))       ...        [L(n), R(n))   +inf
	// ------------------------------------------------------------------->
	rm, rn := rl.searchNearestLeftRange(r.L()), rl.searchNearestLeftRange(r.R())
	if rm.Empty() && rn.Empty() {
		rl.mapping[r.L()] = r
		return nil
	}
	if rm.Equals(rn) {
		rl.mapping[rm.L()] = rm.Merge(r)
		return nil
	}
	mergedM, mergedN := rm.Merge(r), rn.Merge(r)
	if !mergedM.Empty() {
		newKey := mergedM.L()
		rl.mapping[newKey] = mergedM
		if newKey != rm.L() {
			delete(rl.mapping, rm.L())
		}
	}
	if !mergedN.Empty() {
		newKey := mergedN.L()
		rl.mapping[newKey] = mergedN
		if newKey != rn.L() {
			delete(rl.mapping, rn.L())
		}
	}
	removingKeys := rl.findKeysBetween(r.L(), r.R())
	for _, v := range removingKeys {
		delete(rl.mapping, v)
	}
	return nil
}

// Remove removes range r from RangeList rl if no overlapping happens,
// if r overlaps with any range (or multiple ranges) in rl, the intersection(s) will be removed.
func (rl *RangeListMap) Remove(r Range) error {
	rl.mu.Lock()
	defer rl.mu.Unlock()

	if r.Empty() {
		return nil
	}

	if rl.mapping == nil {
		return nil
	}
	// Similar to Add() process, when removing range r from rl, we need to find the 'min range' and
	// 'max range' in rl, then do some cuts and deletions.
	//
	// 'min range': the one with minimum L bound in all overlapping range(s) with r.
	// 'max range': the one with maximum L bound in all overlapping range(s) with r.
	//
	//   1. find the min range in rl overlaps with r, named R[m],
	//   2. find the max range in rl overlaps with r, named R[n],
	//   3. cut the intersection of r and R[m] from R[m],
	//   4. cut the intersection of r and R[n] from R[n],
	//      (if m==n, cut only once)
	//   5. delete elements between >m && <n
	//
	//  -inf     [L(m), R(m))       ...        [L(n), R(n))   +inf
	//               [L(r)..........................R(r))
	// ------------------------------------------------------------------->
	rm, rn := rl.searchNearestLeftRange(r.L()), rl.searchNearestLeftRange(r.R())
	// origin code
	//
	// mcuts := rm.Cut(r)
	// if mcuts == nil {
	// 	delete(rl.mapping, rm.L())
	// } else if len(mcuts) == 1 && !mcuts[0].Equals(rm) {
	// 	delete(rl.mapping, rm.L())
	// 	newKey := mcuts[0].L()
	// 	rl.mapping[newKey] = mcuts[0]
	// } else if len(mcuts) == 2 {
	// 	delete(rl.mapping, rm.L())
	// 	newK1, newK2 := mcuts[0].L(), mcuts[1].L()
	// 	rl.mapping[newK1] = mcuts[0]
	// 	rl.mapping[newK2] = mcuts[1]
	// }
	//
	// simplified code
	//
	// size of mcuts can be 0, 1, 2
	if !rm.Empty() {
		mcuts := rm.Cut(r)
		delete(rl.mapping, rm.L())
		for _, v := range mcuts {
			rl.mapping[v.L()] = v
		}
	}
	if !rn.Empty() && !rm.Equals(rn) {
		// size of cuts can be 0, 1, 2
		ncuts := rn.Cut(r)
		delete(rl.mapping, rn.L())
		for _, v := range ncuts {
			rl.mapping[v.L()] = v
		}
	}
	removingKeys := rl.findKeysBetween(r.L(), r.R())
	for _, v := range removingKeys {
		delete(rl.mapping, v)
	}
	return nil
}

func (rl *RangeListMap) clear() error {
	rl.mapping = nil
	return nil
}

func (rl *RangeListMap) Print() error {
	_, err := fmt.Println(rl.sprint())
	return err
}

func (rl *RangeListMap) sprint() string {
	if len(rl.mapping) == 0 {
		return ""
	}
	// all sorted left bounds
	keys := make([]int, 0, len(rl.mapping))
	for k := range rl.mapping {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})
	// XXX need better estimation
	var b strings.Builder
	b.Grow(16 * len(rl.mapping))

	for i := 0; i < len(keys)-1; i++ {
		b.WriteString(rl.mapping[keys[i]].Sprint() + " ")
	}
	// the last one has no space
	b.WriteString(rl.mapping[keys[len(keys)-1]].Sprint())
	return b.String()
}

// nearestLeftRange searches the map for the most right range in left of value l.
//
// e.g. in the following coordinate, the range n is the nearest left range of l.
//               				  l
//  -inf   [L(n-1), R(n-1))    [L(n), R(n))   [L(n+1), R(n+1))  +inf
// ------------------------------------------------------------------->
func (rl *RangeListMap) searchNearestLeftRange(l int) Range {
	keys := make([]int, 0, len(rl.mapping))
	for k := range rl.mapping {
		keys = append(keys, k)
	}
	lt := searchNearestLeft(keys, l)
	if lt == math.MinInt {
		return Range{}
	}
	return rl.mapping[lt]
}

// findKeysBetween searches the map and returns all L bounds of the non-overlapping ranges
// between m and n (not contains).
//
// e.g. in the following coordinate, L(B), L(C) are the keys between m and n.
//               m..............................................n
//  -inf    [L(A), R(A))    [L(B), R(B))   [L(C), R(C))     [L(D), R(D))      +inf
// -------------------------------------------------------------------------------->
func (rl *RangeListMap) findKeysBetween(m, n int) []int {
	// O(n)
	keys := make([]int, 0, len(rl.mapping))
	for k := range rl.mapping {
		keys = append(keys, k)
	}
	// O(n)
	var lefts []int
	for _, k := range keys {
		if k > m && rl.mapping[k].R() < n {
			lefts = append(lefts, k)
		}
	}
	return lefts
}
