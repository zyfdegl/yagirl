package main

import (
	"math"
	"sort"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// searchNearestLeft finds the maximum integer less than n in slice.
func searchNearestLeft(slice []int, n int) int {
	// O(logN)
	sort.Slice(slice, func(i, j int) bool {
		return slice[i] < slice[j]
	})
	// TODO optimize
	// O(N)
	max := math.MinInt
	for _, v := range slice {
		if v > n {
			break
		}
		max = v
	}
	return max
}
