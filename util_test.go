package main

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMinMax(t *testing.T) {
	var cases = []struct {
		a, b     int
		min, max int
	}{
		{0, 0, 0, 0},
		{1, 5, 1, 5},
		{5, 1, 1, 5},
		{-5, -1, -5, -1},
	}
	for i, v := range cases {
		assert.Equalf(t, v.min, min(v.a, v.b), "min failed on case %d", i)
		assert.Equalf(t, v.max, max(v.a, v.b), "max failed on case %d", i)
	}
}

func TestSearchNearestLeft(t *testing.T) {
	var cases = []struct {
		input []int
		n     int

		expect int
	}{
		{[]int{}, 0, math.MinInt},
		{[]int{10, 20, 30, 40, 50}, 25, 20},
		{[]int{10, 20, 30, 40, 50}, 30, 30},
	}
	for i, v := range cases {
		got := searchNearestLeft(v.input, v.n)
		assert.Equalf(t, got, v.expect, "failed on case %d", i)
	}
}
